---

{% set kernelrelease = grains['kernelrelease'] %}

{% for pkg in pillar['install_pkg_for_make_netatop'] %}
install_{{ pkg }}:
  pkg.installed:
    - name: {{ pkg }}
    - failhard: True
{% endfor %}

downloads_{{ pillar['netatop_version'] }}:
  file.managed:
    - name: /tmp/{{ pillar['netatop_version'] }}.tar.gz
    - source: https://www.atoptool.nl/download/{{ pillar['netatop_version'] }}.tar.gz
    - skip_verify: True
    - require:
        - pkg: install_pkg_for_make_netatop

extract_{{ pillar['netatop_version'] }}:
  archive.extracted:
    - name: /opt
    - source: https://www.atoptool.nl/download/{{ pillar['netatop_version'] }}.tar.gz
    - user: root
    - group: root
    - if_missing: /tmp/{{ pillar['netatop_version'] }}
    - skip_verify: True
    - require:
        - file: downloads_{{ pillar['netatop_version'] }}

make_{{ pillar['netatop_version'] }}:
  cmd.run:
    - cwd: /opt/{{ pillar['netatop_version'] }}
    - name: |
        make
        make install
    - require:
        - archive: extract_{{ pillar['netatop_version'] }}
    - onchanges:
        - archive: extract_{{ pillar['netatop_version'] }}

running_{{ pillar['netatop_version'] }}:
  service.running:
    - name: netatop
    - full_restart: True
    - enable: True
    - watch:
        - cmd: make_{{ pillar['netatop_version'] }}