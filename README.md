**Netatop version:**

_in pillar:_ 

`netatop_version: netatop-3.1`

**Pkg for make netatop** _(kernel release is detected automatically
)_:

_in pillar:_

`install_pkg_for_make_netatop`


**SLS for Ubuntu**

    sudo salt -v '*' state.apply netatop_make_install