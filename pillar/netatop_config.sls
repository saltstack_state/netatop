---

netatop_version: netatop-3.1

install_pkg_for_make_netatop:
  - zlib1g-dev
  - make
  - linux-headers-{{ kernelrelease }}
  - atop
  - build-essential
  - software-properties-common